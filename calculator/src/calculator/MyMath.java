package calculator;

import java.util.Scanner;

import javax.naming.spi.DirStateFactory.Result;

public class MyMath {
	private int A;
	private int B;
	private float resault;
	private String action;

	public MyMath() {
		System.out.println("Calculator running");
	}

	Scanner in = new Scanner(System.in);

	private boolean CheckError(int a) {
		boolean flag = true;
		if (a > 2147483647 || a < -2147483647) {
			System.out.println("ALARMA!!! Number is very big");
			flag = false;
		} else
			flag = true;

		return flag;
	}

	private boolean CheckError(String a) {
		boolean flag = false;
		switch (a) {
		case "+":
			flag=true;
			break;
		case "-":
			flag=true;
			break;
		case "*":
			flag=true;
			break;
		case "/":
			flag=true;
			break;
		default:
			flag=false;
			break;
		}
		return flag;

	}

	public void SetIntegers() {
		boolean flag=true;
		int temp;
		try {
		System.out.println("Enter INTEGER number A");
		temp = in.nextInt();
		A=temp;
		System.out.println("Enter INTEGER number B");
		temp= in.nextInt();
		B=temp;
		flag=false;
		}
		catch (Exception e) {
			System.out.println("Number is very big");
		}
		
	}

	public void SetAction() {
		String c;
		while (true) {
			System.out.println("Please, enter action\n\nIf you want _____ Press _____\nSTACK"
					+ "                     +\nSUBTRACTION               -\n"
					+ "MULTIPLECTION             *\nDIVISION                  / ");
			c = in.nextLine();
			if (CheckError(c)) 
				break;
			
		}
		action=c;
	}

	public String getAction() {
		return action;

	}

	public float Stack() {
		return resault = A + B;
	}

	public float Subtraction() {
		return resault = A - B;
	}

	public float Multiplection() {
		return resault = A * B;
	}

	public float Division() {
		try {
			resault = ((float) A) / ((float) B);
		} catch (Exception e) {
			System.out.println("No result");
		}
		return resault;
	}

	public void getResult() {
		System.out.println(resault);
	}
}