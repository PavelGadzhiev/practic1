package example;

import java.util.Scanner;

public class StringHandler {
	String[] StringArray;
	private String[] stringBuffer;
	Scanner in = new Scanner(System.in);
	public int length;

	public StringHandler(int i) {
		StringArray = new String[i];
		this.length = i;
	}

	public void StringWrite() {
		for (int i = 0; i < StringArray.length; i++) {
			System.out.println("Input string number " + (i + 1));
			StringArray[i] = in.nextLine();
		}
	}

	public int MaxStringLength(String[] arrayString) {
		this.stringBuffer = arrayString;
		int count = 0;
		int max = 0;
		for (int i = 0; i < stringBuffer.length; i++) {
			if (stringBuffer[i].length() > max) {
				max = stringBuffer[i].length();
				count = i;
			}
		}
		System.out.println("Max length " + stringBuffer[count].length());
		System.out.println("Max String: " + stringBuffer[count]);
		return count;
	}

	public String MinStringLength(String[] arrayString) {
		this.stringBuffer = arrayString;
		int count = 0;
		int min = 100;
		for (int i = 0; i < stringBuffer.length; i++) {
			if (stringBuffer[i].length() < min) {
				min = stringBuffer[i].length();
				count = i;
			}
		}

		for (int i = 0; i < stringBuffer.length; i++) {
			if (stringBuffer[i].length() == min) {
				count = i;
				break;
			}
		}
		return stringBuffer[count];
	}

	public int MinStringLength() {
		int count = 0;
		int min = 100;
		for (int i = 0; i < StringArray.length; i++) {
			if (StringArray[i].length() < min) {
				min = StringArray[i].length();
				count = i;
			}
		}
		System.out.println("Min length " + StringArray[count].length());
		System.out.println("Min length: " + StringArray[count]);
		return count;
	}

	public void StringPrint(int i) {
		System.out.println(StringArray[i]);
	}

	public String StringRead(int i) {
		String str;
		str = StringArray[i];
		return str;
	}

	public int Average() {
		int sum = 0;
		int aver = 0;
		for (int i = 0; i < StringArray.length; i++) {
			sum += StringArray[i].length();
		}
		aver = sum / StringArray.length;
		return aver;
	}

	public void PrintMoreAverage(int aver) {
		for (int i = 0; i < StringArray.length; i++) {
			if (StringArray[i].length() > aver) {
				System.out.println(StringArray[i]);
			}
		}
	}

	public void PrintLessAverage(int aver) {
		for (int i = 0; i < StringArray.length; i++) {
			if (StringArray[i].length() < aver) {
				System.out.println(StringArray[i]);
			}
		}
	}

	private String removeCharAt(String s, int pos) {
		return s.substring(0, pos) + s.substring(pos + 1);
	}

	public String LetterCounter(int a) {
		int count = 0;
		String sIn = StringArray[a];
		String sOut = "";
		for (int i = 0; i < StringArray[a].length(); i++) {
			StringBuffer sBuf = new StringBuffer();
			sBuf.append(sIn.charAt(i));
			if (!sOut.contains(sBuf)) {
				sOut += sIn.charAt(i);
			}
		}

		return sOut;
	}

	public boolean uniqueLetters(int a) {
		int count = 0;
		boolean flag;
		for (int i = 0; i < StringArray[a].length(); i++) {
			for (int j = 0; j < StringArray[a].length(); j++) {
				if (i != j) {
					if (StringArray[a].charAt(i) == StringArray[a].charAt(j))

					{
						count++;
					}
				}
			}
		}
		
		return flag=count>0? false:true;

	}

}
