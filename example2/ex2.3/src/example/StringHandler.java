package example;

import java.util.Scanner;

public class StringHandler {
	String [] StringArray;
	Scanner in = new Scanner(System.in);
	public StringHandler(int i)
	{
		StringArray = new String[i];
	}
	public void StringWrite()
	{
		for (int i = 0; i < StringArray.length; i++) {
			System.out.println("Input string number "+(i+1));
			StringArray[i]=in.nextLine();
		}
	}
	public int MaxStringLength()
	{
		int count=0;
		int max=0;
		for (int i = 0; i < StringArray.length; i++) {
			if(StringArray[i].length()>max)
			{
				max=StringArray[i].length();
				count=i;
			}
		}
		System.out.println("Max length "+StringArray[count].length());
		System.out.println("Max length: "+StringArray[count]);
		return count;
	}
	public int MinStringLength()
	{
		int count=0;
		int min=100;
		for (int i = 0; i < StringArray.length; i++) {
			if(StringArray[i].length()<min)
			{
				min=StringArray[i].length();
				count=i;
			}
		}
		System.out.println("Min length "+StringArray[count].length());
		System.out.println("Min length: "+StringArray[count]);
		return count;
	}
	public void StringPrint(int i)
	{
		System.out.println(StringArray[i]);
	}
	public String StringRead(int i) {
		String str;
		str=StringArray[i];
		return str;
	}
	public int Average() {
		int sum=0;
		int aver=0;
		for (int i = 0; i < StringArray.length; i++) {
			sum+=StringArray[i].length();
		}
		aver=sum/StringArray.length;
		return aver;
	}
	public void PrintMoreAverage(int aver) {
		for (int i = 0; i < StringArray.length; i++) {
			if(StringArray[i].length()>aver)
			{
				System.out.println(StringArray[i]);
			}
		}
	}
	public void PrintLessAverage(int aver) {
		for (int i = 0; i < StringArray.length; i++) {
			if(StringArray[i].length()<aver)
			{
				System.out.println(StringArray[i]);
			}
		}
	}
}
