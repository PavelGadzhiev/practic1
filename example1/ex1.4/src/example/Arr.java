package example;

public class Arr {
	
	private int array[];
	
	public Arr(int arr[] ) {
		array=new int[arr.length];
		for(int i=0;i<array.length;i++)
		{
			array[i]=arr[i];
		}
	}
	
	public void printArray()
	{
		for(int i=0;i<array.length;i++)
		{
			System.out.print(array[i]);
		}
	}
	public int SumChet()
	{
		int sum=0;
		for (int i = 0; i < array.length; i++) {
			if(i%2==1)
			{
				sum+=array[i];
			}
		}
		return sum;
	}
	public void Negativ2Zero()
	{
		for (int i = 0; i < array.length; i++) {
			if(array[i]<0)
			{
				array[i]=0;
			}
		}
	}
	public void Triple()
	{
		for (int i = 0; i < array.length; i++) {
			if(array[i]>0)
			{
				if(array[i+1]<0)
				{
					array[i]=array[i]*3;
				}
			}
		}
	}
}
