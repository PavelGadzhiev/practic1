package example;

public class Arr {
	
	private int array[];
	
	public Arr(int arr[] ) {
		array=new int[arr.length];
		for(int i=0;i<array.length;i++)
		{
			array[i]=arr[i];
		}
	}
	
	public void printArray()
	{
		for(int i=0;i<array.length;i++)
		{
			System.out.print(array[i]);
		}
	}
	public int SumChet()
	{
		int sum=0;
		for (int i = 0; i < array.length; i++) {
			if(i%2==1)
			{
				sum+=array[i];
			}
		}
		return sum;
	}
	public void Negativ2Zero()
	{
		for (int i = 0; i < array.length; i++) {
			if(array[i]<0)
			{
				array[i]=0;
			}
		}
	}
	public float Average()
	{
		float aver=0;
		int sum=0;
		for (int i = 0; i < array.length; i++) {
			sum+=array[i];
		}
		aver=(float)sum/(float)array.length;
		return aver;
	}
	public float getMin()
	{
		float a=0;
		for(int i=0;i<array.length;i++)
		{
			if(array[i]<a)
			{
				a=array[i];
			}
		}
		return a;
	}

}
